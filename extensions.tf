resource "azurerm_virtual_machine_extension" "oms_ext" {
  name                       = "oms"
  location                   = "${var.location}"
  resource_group_name        = "${var.resource_group}"
  virtual_machine_name       = "${azurerm_virtual_machine.jumpbox_host.name}"
  publisher                  = "Microsoft.EnterpriseCloud.Monitoring"
  type                       = "MicrosoftMonitoringAgent"
  type_handler_version       = "1.0"
  auto_upgrade_minor_version = true

  settings = <<SETTINGS
  {
    "workspaceId": "${var.oms_workspace_id}"
  }
SETTINGS

  protected_settings = <<SETTINGS
  {
    "workspaceKey": "${var.oms_workspace_key}"
  }
SETTINGS
}

resource "azurerm_virtual_machine_extension" "Malware_Protection_Extension" {
  name                 = "IaaSAntimalware"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group}"
  virtual_machine_name = "${azurerm_virtual_machine.jumpbox_host.name}"
  publisher            = "Microsoft.Azure.Security"
  type                 = "IaaSAntimalware"
  type_handler_version = "1.5"

  settings = <<SETTINGS
  { 
    "AntimalwareEnabled": true, 
    "RealtimeProtectionEnabled": true, 
    "ScheduledScanSettings": { 
      "isEnabled": true, 
      "day": "1", 
      "time": "120", 
      "scanType": "Full"
    }, 
    "Exclusions": { 
      "Extensions": ".mdf;.ldf", 
      "Paths": "c:\\windows;c:\\windows\\system32", 
      "Processes": "taskmgr.exe;notepad.exe" 
    } 
  }
SETTINGS
}

data "template_file" "xml" {
  template = "${file("${path.module}/files/windows-config.xml")}"
}

resource "azurerm_virtual_machine_extension" "vmdiagjump" {
  name                 = "vmdiag"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group}"
  virtual_machine_name = "${azurerm_virtual_machine.jumpbox_host.name}"
  publisher            = "Microsoft.Azure.Diagnostics"
  type                 = "IaaSDiagnostics"
  type_handler_version = "1.5"

  settings = <<SETTINGS
  {
    "xmlCfg": "${base64encode(data.template_file.xml.rendered)}",
    "storageAccount": "${var.diagstorageaccountname}"
  }
SETTINGS

  protected_settings = <<SETTINGS
  {
    "storageAccountName": "${var.diagstorageaccountname}",
    "storageAccountKey": "${var.diagstorageaccountkey}"
  }
SETTINGS
}

data "template_file" "language_ps1" {
  template = "${file("${path.module}/files/language.ps1")}"
}

resource "azurerm_virtual_machine_extension" "custscriptext" {
  name                 = "customscriptext"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group}"
  virtual_machine_name = "${azurerm_virtual_machine.jumpbox_host.name}"
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.7"

  protected_settings = <<SETTINGS
  {
    "commandToExecute": "powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64encode(data.template_file.language_ps1.rendered)}')) | Out-File -filepath language.ps1\" && powershell -ExecutionPolicy Unrestricted -File language.ps1"
  }
SETTINGS
}

data "azurerm_subscription" "current" {}

resource "azurerm_key_vault_key" "jumpbox_encryption_key" {
  name         = "jumpbox-disk-encryption"
  key_vault_id = "${var.keyvault_vault_id}"
  key_type     = "RSA"
  key_size     = 2048

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}

resource "azurerm_virtual_machine_extension" "disk-encryption" {
  name                 = "DiskEncryption"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group}"
  virtual_machine_name = "${azurerm_virtual_machine.jumpbox_host.name}"
  publisher            = "Microsoft.Azure.Security"
  type                 = "AzureDiskEncryption"
  type_handler_version = "2.2"

  settings = <<SETTINGS
  {
    "EncryptionOperation": "EnableEncryption",
    "KeyVaultURL": "https://${var.keyvault_vault_name}.vault.azure.net",
    "KeyVaultResourceId": "/subscriptions/${data.azurerm_subscription.current.subscription_id}/resourceGroups/${var.resource_group}/providers/Microsoft.KeyVault/vaults/${var.keyvault_vault_name}",
    "KeyEncryptionKeyURL": "https://${var.keyvault_vault_name}.vault.azure.net/keys/${azurerm_key_vault_key.jumpbox_encryption_key.name}/${azurerm_key_vault_key.jumpbox_encryption_key.version}",
    "KekVaultResourceId": "/subscriptions/${data.azurerm_subscription.current.subscription_id}/resourceGroups/${var.resource_group}/providers/Microsoft.KeyVault/vaults/${var.keyvault_vault_name}",
    "KeyEncryptionAlgorithm": "RSA-OAEP",
    "VolumeType": "All"
  }
SETTINGS
}
