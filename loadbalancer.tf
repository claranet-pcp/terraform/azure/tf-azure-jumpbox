resource "azurerm_public_ip" "jump_public_ip" {
  name                         = "${var.resource_prefix}-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group}"
  public_ip_address_allocation = "static"
  domain_name_label            = "jump${var.customer}${var.environment}"
}

resource "azurerm_lb" "jumpbox" {
  name                = "${var.resource_prefix}-elb"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group}"

  frontend_ip_configuration {
    name                 = "${var.resource_prefix}-elb-ipconfig"
    public_ip_address_id = "${azurerm_public_ip.jump_public_ip.id}"
  }
}

resource "azurerm_lb_rule" "jumpbox" {
  resource_group_name            = "${var.resource_group}"
  loadbalancer_id                = "${azurerm_lb.jumpbox.id}"
  name                           = "LBRule"
  protocol                       = "Tcp"
  frontend_port                  = 443
  backend_port                   = 3389
  frontend_ip_configuration_name = "${var.resource_prefix}-elb-ipconfig"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.jumpbox.id}"
  probe_id                       = "${azurerm_lb_probe.jumpbox.id}"
  depends_on                     = ["azurerm_lb_probe.jumpbox"]
}

resource "azurerm_lb_backend_address_pool" "jumpbox" {
  resource_group_name = "${var.resource_group}"
  loadbalancer_id     = "${azurerm_lb.jumpbox.id}"
  name                = "BackEndAddressPool"
}

resource "azurerm_lb_probe" "jumpbox" {
  resource_group_name = "${var.resource_group}"
  loadbalancer_id     = "${azurerm_lb.jumpbox.id}"
  name                = "rdp-running-probe"
  port                = 3389
}
