variable "resource_prefix" {}

variable "azure_location_short" {}

variable "customer" {}

variable "environment" {}

variable "location" {}

variable "username" {}

variable "public_subnets" {}
variable "public_subnets_prefix" {}

variable "instance_number" {}

variable "vm_size" {
  default = "Standard_B2s"
}

variable "resource_group" {}

variable "keyvault_vault_uri" {}

variable "keyvault_vault_id" {
  
}
variable "keyvault_vault_name" {}

variable "oms_workspace_id" {}
variable "oms_workspace_key" {}

variable "diagstorageaccountname" {}

variable "diagstorageaccountkey" {}
