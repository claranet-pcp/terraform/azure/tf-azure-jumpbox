resource "random_string" "jump_rndstr" {
  length  = 16
  special = true
  upper   = true
  lower   = true
  number  = true
}

resource "azurerm_key_vault_secret" "jump_password" {
  name      = "${var.resource_prefix}-jump-${var.username}"
  value     = "${random_string.jump_rndstr.result}"
  vault_uri = "${var.keyvault_vault_uri}"
}

resource "azurerm_network_interface" "jumpbox_network_interface" {
  name                = "${var.resource_prefix}-jump-interface"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group}"

  ip_configuration {
    name                                    = "${var.resource_prefix}-jump-ipconfig"
    subnet_id                               = "${element(split(",",var.public_subnets),0)}"
    private_ip_address_allocation           = "static"
    private_ip_address                      = "${cidrhost(element(split(",",var.public_subnets_prefix),0),5 )}"
    load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.jumpbox.id}"]
  }

  tags {
    service     = "jumpbox"
    terraform   = "true"
    environment = "${var.environment}"
  }
}

resource "azurerm_virtual_machine" "jumpbox_host" {
  name                             = "${var.resource_prefix}-jump"
  location                         = "${var.location}"
  resource_group_name              = "${var.resource_group}"
  network_interface_ids            = ["${azurerm_network_interface.jumpbox_network_interface.id}"]
  vm_size                          = "${var.vm_size}"                                              # Never A0!!! Maybe switch to F2 -only in certain regions
  delete_data_disks_on_termination = true
  delete_os_disk_on_termination    = true

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.resource_prefix}-jump-OS"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.environment}jump"
    admin_username = "${var.username}"
    admin_password = "${random_string.jump_rndstr.result}"
  }

  os_profile_windows_config {
    provision_vm_agent        = true
    enable_automatic_upgrades = true
  }

  tags {
    service     = "jumpbox"
    terraform   = "true"
    environment = "${var.environment}"
  }
}
