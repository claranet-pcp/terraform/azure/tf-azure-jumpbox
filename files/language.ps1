$config = '
<gs:GlobalizationServices xmlns:gs="urn:longhornGlobalizationUnattend"> 
<!--User List-->
<gs:UserList>
    <gs:User UserID="Current" CopySettingsToDefaultUserAcct="true" CopySettingsToSystemAcct="true"/> 
</gs:UserList>
<!-- user locale -->
<gs:UserLocale> 
    <gs:Locale Name="en-GB" SetAsCurrent="true"/> 
</gs:UserLocale>
<!-- system locale -->
<gs:SystemLocale Name="en-GB"/>
<!-- GeoID -->
<gs:LocationPreferences> 
    <gs:GeoID Value="242"/> 
</gs:LocationPreferences>
<gs:MUILanguagePreferences>
	<gs:MUILanguage Value="en-GB"/>
	<gs:MUIFallback Value="en-US"/>
</gs:MUILanguagePreferences>
<!-- input preferences -->
<gs:InputPreferences>
    <!--en-GB-->
    <gs:InputLanguageID Action="add" ID="0809:00000809" Default="true"/> 
</gs:InputPreferences>
</gs:GlobalizationServices>
'
$config | Out-file -filepath ukregion.xml -Encoding ASCII


# Set Locale, language etc. 
& $env:SystemRoot\System32\control.exe "intl.cpl,,/f:`"UKRegion.xml`""

# These would be nice but they don't affect the default user account, so are pretty pointless.
Set-WinSystemLocale -SystemLocale en-GB
Set-WinHomeLocation -GeoId 242
Set-WinUserLanguageList -LanguageList (New-WinUserLanguageList -Language en-GB) -Force

# Set Timezone
& tzutil /s "GMT Standard Time"

# Set languages/culture
Set-Culture en-GB # This one works!
